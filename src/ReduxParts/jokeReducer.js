import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";

// thunk stuff
export const getJokeAsync = createAsyncThunk(
    'joke/getJokeAsync',
    async () => {
        // fetch a joke
        const response = await fetch("https://v2.jokeapi.dev/joke/Any?blacklistFlags=nsfw,religious,political,racist,sexist,explicit&type=twopart")

        if (response.ok) {
            const result = await response.json()
            console.log("jokeReducer.js getJokeASync(): ", result);
            return result
        }
    }
)

// our slice
export const jokeSlice = createSlice({
    name: 'joke',
    initialState: {
            setup: "This is the setup",
            delivery: "and this is the delivery."
    },
    reducers: {

    },
    extraReducers: {
        [getJokeAsync.fulfilled]: (state, action) => {
            console.log("jokeReducer.js extraReducer action.payload: ", action.payload);
            state.setup = action.payload.setup
            state.delivery = action.payload.delivery
        }
    }
})

export default jokeSlice.reducer