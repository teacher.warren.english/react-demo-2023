import { configureStore } from "@reduxjs/toolkit";
import countReducer from "./countReducer";
import jokeReducer from "./jokeReducer";

export default configureStore ({
    reducer: {
        counter: countReducer,
        joke: jokeReducer
    }
})