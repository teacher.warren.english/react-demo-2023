import './App.css';
import DogPage from './Pages/DogPage';
import MessageProvider from './Contexts/MessageProvider';
import JokePage from './Pages/JokePage';
import { BrowserRouter, Routes, Route, Navlink, NavLink } from 'react-router-dom'
import NavBar from './Components/NavBar';
import CountPage from './Pages/CountPage';

function App() {


  const myFavorites = {
    color: "purple",
    music: ["metal", "hip-hop", "hardstyle", "acid-jazz", "melodic viking hero indie reggae"],
    display: true,
  }

  return (
    <div className="App">
      <BrowserRouter>
        <NavBar />
        <MessageProvider>
          <Routes>
            <Route path='/' element={<DogPage />} />
            <Route path='/jokes' element={<JokePage />} />
            <Route path='/count' element={<CountPage />} />
            <Route path='*' element={
              <>
                <h1>There's nothing here 👻</h1>
                <NavLink to="/">Return Home</NavLink>
              </>
            } />

          </Routes>
        </MessageProvider>
      </BrowserRouter>
    </div>
  );
}

export default App;
