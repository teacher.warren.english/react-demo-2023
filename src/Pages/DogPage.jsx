import React, { useEffect, useState } from 'react'
import Dog from '../Components/Dog'

function DogPage() {

    const API_URL = "https://dog.ceo/api/breeds/image/random"
    const [dogPictureUrl, setDogPicture] = useState("") // REACT Hook


    let pieceOfInfo = "Here's something!"

    useEffect(() => {
        getFromAPI()
        // console.log("Dog image URL: ", );
    }, [])

    function getFromAPI() {
        fetch(API_URL)
            .then(response => response.json())
            .then(data => {
                console.log(dogPictureUrl);
                setDogPicture(data.message)
            })

    }

    return (
        <Dog dogPicture={dogPictureUrl} someInfo={pieceOfInfo}></Dog>
    )
}

export default DogPage