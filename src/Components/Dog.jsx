import { useContext } from "react";
import { MessageContext } from "../Contexts/MessageProvider";

function Dog(props) {

    // const [message, setMessage] = useState("The message")
    const [message, setMessage] = useContext(MessageContext)

    return (
        <>
            <h1>Click the button below to get a dog!</h1>
            <button>Get Dog Image</button>
            <div>
                <img src={props.dogPicture} alt="doggo" />
                <p>{props.someInfo}</p>
                <h4>{message}</h4>
            </div>
        </>
    )
}

export default Dog