import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { boost, decrement, increment } from '../ReduxParts/countReducer';

function Count() {

    const count = useSelector((state) => state.counter.value);
    // const state = useSelector(state => state.counter)

    const dispatch = useDispatch()

    function test() {
        console.log(increment);
    }

    return (
        <div>
            <h1>Counter:</h1>
            <h3>{count}</h3>
            <button onClick={() => dispatch(increment())}>Increase!</button>
            <button onClick={() => dispatch(decrement())}>Decrease!</button>
            <button onClick={() => dispatch(boost(15))}>Boost!</button>
            <button onClick={test}>Test</button>
        </div>
    )
}

export default Count