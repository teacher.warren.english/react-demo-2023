import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { getJokeAsync } from '../ReduxParts/jokeReducer';

function Joke() {

  const dispatch = useDispatch()
  
  const joke = useSelector(state => state.joke)

  // get the first joke when the page loads
  useEffect(() => {
    handleGetNewJoke()
  }, [])

  function handleGetNewJoke() {
    // get joke stuff
    // dispatch our thunk (getJokes)
    dispatch(getJokeAsync())
  }

  return (
    <>
      {joke && <h3>{joke.setup}</h3>}
      {joke && <p>{joke.delivery}</p>}
      <button onClick={handleGetNewJoke}>Get another joke</button>
    </>
  )
}

export default Joke