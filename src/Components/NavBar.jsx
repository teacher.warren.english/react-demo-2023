import React from 'react'
import { NavLink } from 'react-router-dom'

function NavBar() {
    return (
        <nav>
            <NavLink to="/"><li>Home</li></NavLink>
            <NavLink to="/jokes"><li>Jokes</li></NavLink>
            <NavLink to="/count"><li>Counting</li></NavLink>
        </nav>
    )
}

export default NavBar